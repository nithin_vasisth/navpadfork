function grid_params = get_grid_params( id )
%GET_GRID_PARAMS Summary of this function goes here
%   Detailed explanation goes here

grid_params = struct();

switch id
    case 1
        grid_params.p_occ_default = 0.5;                                %default occupancy value
        grid_params.p_empty_default = 1 - grid_params.p_occ_default;
        grid_params.p_occ_sensor = 0.95;                                %occupancy increase on hit
        grid_params.p_empty_sensor = 0.60;                              %occupancy decrease on miss
    case 'exploration_grid'
        grid_params.p_occ_default = 0.2;                                %default occupancy value
        grid_params.p_empty_default = 1 - grid_params.p_occ_default;
        grid_params.p_occ_sensor = 0.95;                                %occupancy increase on hit
        grid_params.p_empty_sensor = 0.60;                              %occupancy decrease on miss
        grid_params.p_occ_neighbor = 0.5;                               %occupancy decrease on miss
    otherwise
        error('Grid parameters requested not available')
end

% Pre-Calculating grid update numbers
grid_params.log_odds_occ = log(grid_params.p_occ_default/grid_params.p_empty_default);
grid_params.log_odds_occ_sensor = log(grid_params.p_occ_sensor/(1-grid_params.p_occ_sensor));
grid_params.log_odds_empty_sensor = log((1-grid_params.p_empty_sensor)/grid_params.p_empty_sensor);

end

