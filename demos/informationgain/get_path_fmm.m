function [ path, valid_path ] = get_path_fmm( fmm_map, start_coord, goal_coord )
%GET_PATH_FMM Summary of this function goes here
%   Detailed explanation goes here
starting_id = coord2index(start_coord(1),start_coord(2),fmm_map.scale,fmm_map.min(1),fmm_map.min(2));
goal_id = coord2index(goal_coord(1),goal_coord(2),fmm_map.scale,fmm_map.min(1),fmm_map.min(2));
current_id = goal_id;
gradient = -1;
path_ids = [];
path = [];
valid_path = 0;

while(gradient<0)
    value = fmm_map.data(current_id(1),current_id(2));
    min_value = value;
    next_id = [];
    for i=-1:1
        for j=-1:1
            id = current_id + [i,j];
            id = max([id;1,1]);
            id = min([id;size(fmm_map.data)]);
            new_value = fmm_map.data(id(1),id(2));
            if(new_value<min_value && ~isinf(new_value))
                next_id = id;
                min_value = new_value;
            end
        end
    end
    gradient = min_value - value;
    if(~isempty(next_id))
        [current_coord(1),current_coord(2)] = index2coord(current_id,fmm_map.scale,fmm_map.min(1),fmm_map.min(2));
        [next_coord(1),next_coord(2)] = index2coord(next_id,fmm_map.scale,fmm_map.min(1),fmm_map.min(2));
        heading = atan2(next_coord(2)-current_coord(2),next_coord(1)-current_coord(1));
        current_id = next_id;
        curr_id = [current_id,heading]; 
        path_ids = [path_ids;curr_id];
    end
end

goal_distance = starting_id - current_id;
goal_distance = sum(goal_distance.*goal_distance,2);
if(goal_distance<4)
    valid_path=1;
    [path_ids(:,1),path_ids(:,2)] = index2coord(path_ids,fmm_map.scale,fmm_map.min(1),fmm_map.min(2));
    path.x = path_ids(:,1)';
    path.y = path_ids(:,2)';
    path.x = fliplr(path.x);
    path.y = fliplr(path.y);
    path.psi= path_ids(:,3)';
    path.psi= fliplr(path.psi)' + pi;
end
end

