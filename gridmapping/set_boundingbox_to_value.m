function [ map ] = set_boundingbox_to_value( value, map, translate, bounding_box, map_struct)
%SETBOUNDINGBOX Summary of this function goes here
%   Detailed explanation goes here
bounding_box = bsxfun(@plus,bounding_box,translate);
ID = coord2index(bounding_box(:,1),bounding_box(:,2),map_struct.scale,map_struct.min(1),map_struct.min(2));
[X,Y] = meshgrid(bounding_box(1,1):bounding_box(2,1),bounding_box(1,2):bounding_box(2,2));
X = X(:); Y = Y(:);
X(:,1) = min(size(map, 1)*ones(size(X(:,1))), max(ones(size(X(:,1))), X(:,1)));
Y(:,1) = min(size(map, 2)*ones(size(Y(:,1))), max(ones(size(Y(:,1))), Y(:,1)));

IND = sub2ind(size(map),X,Y);
map(IND(:)) = value;
end

