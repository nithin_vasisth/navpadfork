function info_gain =  estimate_info_gain(grid_map,grid_params,map_struct,pose,laser_params)
% sensor_model.p_occ_occ sensor_model.p_occ_unocc
% sensor_model.p_unocc_occ sensor_model.p_unocc_unocc

info_gain=zeros(size(pose,1),1);
angles = laser_params.laser_fov(1):laser_params.laser_resolution:laser_params.laser_fov(2);
ranges = [0:laser_params.range_resolution:laser_params.max_range]';

% grid_params.p_occ_default = 0.5;                                %default occupancy value
% grid_params.p_empty_default = 1 - grid_params.p_occ_default;  
% grid_params.p_occ_sensor = 0.95;                                %occupancy increase on hit
% grid_params.p_empty_sensor = 0.60;                              %occupancy decrease on miss


for j=1:size(pose,1)
    angles2 = bsxfun(@plus, angles,pose(j,3));
    
    for i=1:size(angles,2)        
        xy = bsxfun(@plus,pose(j,1:2),bsxfun(@times,ranges,[cos(angles2(i)),sin(angles2(i))]));
        xy = coord2index(xy(:,1),xy(:,2),map_struct.scale,map_struct.min(1),map_struct.min(2));
        
        xy(:,1) = min(size(map_struct.data, 1)*(ones(size(xy(:,1)))), max((ones(size(xy(:,1)))), xy(:,1)));
        xy(:,2) = min(size(map_struct.data, 2)*(ones(size(xy(:,2)))), max((ones(size(xy(:,2)))), xy(:,2)));
        ind = sub2ind(size(map_struct.data),xy(:,1),xy(:,2));
        ind = unique(ind);
        
        [ expected_entropy, probability_ray_passes ] = calculate_expected_entropy_cell(grid_map(ind),grid_params );
           
        p = [1; probability_ray_passes];
        p(end) = [];
        expected_entropy = expected_entropy.*cumprod(p);
        expected_entropy = sum(expected_entropy);
        info_gain(j) = info_gain(j) + expected_entropy;
    end
end
