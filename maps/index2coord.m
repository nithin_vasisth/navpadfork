function [x,y] = index2coord(ID,resolution,minx,miny)
%COORD2INDEX Summary of this function goes here
%   Detailed explanation goes here
x = (ID(:,1)-1)*resolution+minx;
y = (ID(:,2)-1)*resolution+miny;
end