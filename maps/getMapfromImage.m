function [ map ] = getMapfromImage( imagePath )
%GETMAPFROMIMAGE Summary of this function goes here
%   Detailed explanation goes here

im = imread(imagePath);
if(size(im,3)>1)
    im = rgb2gray(im);
end
map = double(im)/double(max(im(:)));
end
