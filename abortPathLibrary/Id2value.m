function [ value ] = Id2value( id,vMin,vResolution )
%ID2VALUE Summary of this function goes here
%   Detailed explanation goes here
value = bsxfun(@minus,bsxfun(@plus,bsxfun(@times,id,vResolution),vMin),vResolution/2);

end