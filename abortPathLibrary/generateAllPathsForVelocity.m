function [ X,Y,T ] = generateAllPathsForVelocity(vMax,roll,rollResolution,rollMaxAngle,rollRate,decceleration,vMin,reactionTime)
X = [];
Y = [];
T = [];

for i=0:rollResolution:rollMaxAngle
    %[x,y,t] = generateChangingCurvatureTrajectory2(vMax,i,decceleration,rollRate);
    [x,y,t,~,~] = generateChangingCurvatureTrajectory2(vMax,roll,i,decceleration,rollRate,vMin,reactionTime);
    X = [X;x];
    Y = [Y;y];
    T = [T;t];
    X = [X;x];
    Y = [Y;-y];
    T = [T;t];
end

